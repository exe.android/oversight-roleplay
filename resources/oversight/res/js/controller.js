﻿var mainBrowser = null;
var lastUpdate = 0;

API.onResourceStart.connect(function () {
    // init browser
    var res = API.getScreenResolution();
    mainBrowser = API.createCefBrowser(res.Width, res.Height);
    API.waitUntilCefBrowserInit(mainBrowser);
    API.setCefBrowserPosition(mainBrowser, 0, 0);
    API.loadPageCefBrowser(mainBrowser, "res/html/login.html");
});

API.onResourceStop.connect(function () {
    // destroy browser
    if (mainBrowser != null) {
        API.destroyCefBrowser(mainBrowser);
    }
});

API.onKeyDown.connect(function (sender, args) {
    if (args.KeyCode == Keys.F12 && mainBrowser != null) {
        API.setCefBrowserHeadless(mainBrowser, !API.getCefBrowserHeadless(mainBrowser));
        API.sendNotification("TEST");
    }
});

API.onUpdate.connect(function ()
{
    API.sendNotification("TEST");
});