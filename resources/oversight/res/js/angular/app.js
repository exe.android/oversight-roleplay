﻿var oversight = angular.module('oversight', []);

angular.element(document).ready(function () {
    var appElement = document.querySelector('[ng-app=oversight]');
    var appScope = angular.element(appElement).scope();

    setVar = function (name, value) {
        appScope.$apply(function () {
            appScope[name] = value;
        })
    };

    onLogInResult = function (result) {
        if (!result)
            appScope.$apply(function () {
                appScope.logInResult = false;
        });
    };

    registerResult = function (result, errorMessage) {
        appScope.$apply(function () {
            if (result == false)
                appScope.regData.errorMessage = errorMessage;
        });
    }
});